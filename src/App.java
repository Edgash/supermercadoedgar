public class App {

    public static void main(String[] args) {

        MyBuffer myBuffer = new MyBuffer(6);

        CajaAutomatica caja1 = new CajaAutomatica(myBuffer,1,1,100,false);
        CajaAutomatica caja2 = new CajaAutomatica(myBuffer,2,1,100,false);
        CajaAutomatica caja3 = new CajaAutomatica(myBuffer,3,1,100,false);
        CajaAutomatica caja4 = new CajaAutomatica(myBuffer,4,1,100,false);
        Cliente cliente1 = new Cliente(myBuffer, 1);
        Cliente cliente2 = new Cliente(myBuffer, 2);

        Thread cajaN1 = new Thread(caja1);
        Thread cajaN2 = new Thread(caja2);
        Thread cajaN3 = new Thread(caja3);
        Thread cajaN4 = new Thread(caja4);
        Thread clienteN1 = new Thread(cliente1);
        Thread clienteN2 = new Thread(cliente2);

        cajaN1.start();
        cajaN2.start();
        cajaN3.start();
        cajaN4.start();
        clienteN1.start();
        clienteN2.start();

        try {
            cajaN1.join();
            cajaN2.join();
            cajaN3.join();
            cajaN4.join();
            clienteN1.join();
            clienteN2.join();

        } catch (InterruptedException e) {
            System.err.println(e.getLocalizedMessage());
        }

    }

}
