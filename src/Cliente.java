
public class Cliente implements Runnable {

    MyBuffer myBuffer;
    int numCliente = -1;

    public Cliente(MyBuffer myBuffer, int numCliente) {
        this.myBuffer = myBuffer;
        this.numCliente = numCliente;
    }

    public Cliente(MyBuffer myBuffer) {
        this.myBuffer = myBuffer;
    }

    @Override
    public void run() {

        while (true){

            int i = myBuffer.get();
            System.out.printf("Cliente %d paga: %d%s \n", numCliente, i, System.lineSeparator());

        }
    }
}
