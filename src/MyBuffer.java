
import java.util.ArrayList;

public class MyBuffer {

    public int value;
    private ArrayList<Integer> buffer = new ArrayList<>();
    private int MAX_SIZE;

    public MyBuffer(int MAX_SIZE){
        this.MAX_SIZE = MAX_SIZE;
    }

    synchronized int get(){
        while(buffer.size() <= 0){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        notifyAll();
        value = buffer.get(0);
        buffer.remove(0);
        return value;
    }

    synchronized void put(int i){
        while (buffer.size() >= MAX_SIZE){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        value = i;
        buffer.add(i);
        notifyAll();
    }
}