
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;

public class CajaAutomatica implements Runnable {

    int minIntNumber;
    int maxIntNumber;
    boolean repeatRandomNumbers;
    final int MIN_SLEEP_MS = 0;
    final int MAX_SLEEP_MS = 100;

    private int countDinero, countClientes;

    MyBuffer myBuffer;
    int numCaja = -1;

    public CajaAutomatica(MyBuffer myBuffer, int numCaja, int minIntNumber,int maxIntNumber, boolean repeatRandomNumbers) {
        this.numCaja = numCaja;
        this.myBuffer = myBuffer;
        this.minIntNumber = minIntNumber;
        this.maxIntNumber = maxIntNumber;
        this.repeatRandomNumbers = repeatRandomNumbers;
    }

    public CajaAutomatica(MyBuffer myBuffer,int numCaja, int minIntNumber,int maxIntNumber){
        this(myBuffer,numCaja,minIntNumber,maxIntNumber,false);
    }

    /**
     * @since 1.0.1
     * @param myBuffer
     * @param numCaja
     */
    public CajaAutomatica(MyBuffer myBuffer, int numCaja){
        this(myBuffer,numCaja,0,10,true);
    }

    @Override
    public void run() {

        ArrayList<Integer> aleatorios = new ArrayList<>();

        IntStream intGenerator = ThreadLocalRandom.current().ints(minIntNumber,maxIntNumber);

        if (!repeatRandomNumbers) {

            intGenerator = intGenerator.distinct();

        }
        intGenerator.forEach(

                (i)->{

                    int sleep = ThreadLocalRandom.current().nextInt(MIN_SLEEP_MS,MAX_SLEEP_MS);

                    try {
                        Thread.sleep(sleep);
                    } catch (InterruptedException e) {
                        System.err.println(e.getLocalizedMessage());
                    }
                    myBuffer.put(i);
                    System.out.printf("Caja %d cobra: %d%s", numCaja, i, System.lineSeparator());
                    recuento(i);
                }
        );
    }

    private void recuento(int i) {
        switch (numCaja){
            case 1:
                countClientes++;
                countDinero = countDinero + i;
                break;
            case 2:
                countClientes++;
                countDinero = countDinero + i;
                break;
            case 3:
                countClientes++;
                countDinero = countDinero + i;
                break;
            case 4:
                countClientes++;
                countDinero = countDinero + i;
                break;
        }
        System.out.println("La caja " + i + " ha atendido a " + countClientes + " clientes.");
        System.out.println("La caja " + i + " ha conseguido " + countDinero + " euros.");
    }
}
